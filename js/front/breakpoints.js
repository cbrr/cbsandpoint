function createBreakpoints(element, arr) {
    var containerWidth = element.width();
    if(typeof element.attr('class') !== 'undefined') {
        element.attr('class', function(i,c){
            return c.replace(/(^|\s)breakpoint-\S+/g, '');
        });
    }

    for(i = 0; i < arr.length; i++) {
        if(containerWidth <= arr[i]) {
            element.addClass('breakpoint-max-' + arr[i]);
        } else {
            element.addClass('breakpoint-min-' + arr[i]);
        }
    }
}