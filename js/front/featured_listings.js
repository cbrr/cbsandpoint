$('.cvdo-featured-listings-wrapper').each(function() {
    var fl = $(this);
    var flIDX = fl.attr('data-fl-idx');
    var flSiteId = fl.attr('data-fl-site-id');
    var flType = fl.attr('data-fl-type');
    var flCount = fl.attr('data-fl-count');
    var flCols = fl.attr('data-fl-cols');
    var flRows = fl.attr('data-fl-rows');
    var flCustomLinkId = 'undefined' !== typeof fl.attr('data-fl-custom-link-id') ? fl.attr('data-fl-custom-link-id') : '';
    var flPauseTime = 'undefined' !== typeof fl.attr('data-fl-pause-time') && fl.attr('data-fl-pause-time') ? fl.attr('data-fl-pause-time') : 8000;

    var flUrl = location.protocol 
    + '//' 
    + flIDX 
    + '/agent_specific_featured_json.php?site_id='
    + flSiteId
    + '&list_count='
    + flCount
    + '&link_id='
    + flCustomLinkId;

    var flTemplate = fl.find('.cvdo-featured-listings-template');
    
    $.ajax({
        url: flUrl,
        dataType: 'jsonp',
    }).always(function() {
        var flListings = listing_data.listings;
        var flNumberOfListings = 30;
        
        if('undefined' !== typeof flCols
        && flCols
        && 'undefined' !== typeof flRows
        && flRows) {
            flNumberOfListings = flCols * flRows;
        }
        
        flTemplate.each(function() {
            var flListingsHTML = "";
            var flTemplateHTML = $(this).html();
            
            $.each(flListings, function(i, d) {
                if(i < flNumberOfListings) {
                    var flData = {
                        listingNumber: d.mls,
                        propType: d.property_type,
                        price: d.price,
                        priceFormatted: d.price.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"),
                        beds: d.beds,
                        baths: d.baths,
                        sqft: d.sqft,
                        acres: d.acres,
                        address: d.address,
                        unit: d.unit,
                        city: d.city,
                        remarks: d.remarks,
                        agentName: d.agent_name,
                        officeName: d.office_name,
                        mainImage: d.main_image,
                        listingUrl: d.listing_url,
                        unit: d.unit,
                    }

                    var flListingHTML = flTemplateHTML;

                    $.each(flData, function(k, v) {
                        var replacementValue = '{{' + k + '}}|%7B%7B' + k + '%7D%7D';
                        var replacementReg = new RegExp(replacementValue, 'igm');

                        flListingHTML = flListingHTML.replace(replacementReg, v);
                    });

                    flListingsHTML += flListingHTML;
                } else {
                    return false;
                }
            });
            
            $(this).after(flListingsHTML).remove();
        });
        
        if('undefined' !== typeof flType 
        && flType) {
            var flStartFunc = 'startFeaturedListings' + flType;
            
            if('undefined' !== typeof window[flStartFunc]) {
                //Call the Individual Start Function For the FL
                var fl = new window[flStartFunc]({
                    pauseTime: flPauseTime
                });

                fl.start();
            }
        }
        
        
    });
});

